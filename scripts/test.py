# coding=utf-8
# Python 3

import unittest
from sgt2plain import formatText

class TestStringMethods(unittest.TestCase):
    def test_formatText(self):
        self.assertEquals("abc efg.", formatText(["abc", "efg", "."]))
        self.assertEquals("abc efg!", formatText(["abc", "efg", "!"]))
        self.assertEquals("abc efg...", formatText(["abc", "efg", ".", ".", "."]), )
        self.assertEquals("abc: efg!", formatText(["abc", ":", "efg", "!"]))
        self.assertEquals("abc (efg hi), jk", formatText(["abc", "(", "efg", "hi", ")", ",", "jk"]))
        self.assertEquals("ab \"cd ef\" gh \"ij kl\", mn", formatText(["ab", '"', "cd", "ef", '"', "gh", '"', "ij", "kl", '"', ",", "mn"]))

if __name__ == '__main__':
    unittest.main()