# README #

Czesl data.

Should eventually include all Czesl data. 


* sgt - SGT version of the corpus with all transcribed data in Manatee format.  The master copy is in [Lindat](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-162).
* from-sgt - sgt file converted int plain text format, split into individual documents. Most of the documents are accompanies with metada. Note: paragraph boundaries and certain other formatting, whitespace aspects of the original files are missing
* main
     * transcribed czesl documents (*.html)
     * the corresponding T0 in PML (*.w.xml) - tokenized, with author's edit inlined (Note that there are some errors in this inlining). The conversion log is in html2pml.log; problematic files were not converted and are in the errors and warnings subdirectories
     * the default T1 layer (*.a.xml) and an empty T2 layer (*.b.xml) - this should be eventually replaced with the manually annotated version, currently in [https://bitbucket.org/czesl/czesl-man](https://bitbucket.org/czesl/czesl-man)