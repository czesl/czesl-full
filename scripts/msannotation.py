# coding=utf-8
# Python 3

from typing import List, NamedTuple

"""

Input data (documents must be in the same order):
-- Manual Morph vertical -> doc/para boundaries, T0 tokens, lemmas/tag - MERGED 
-- Auto Morphodita vertical -> auto lemma/tag/syntax
-- brat plain+brat annot

Output - Vertical format containing
-- doc/para boundaries as xml tags
-- T0 tokeny
-- Manual & Automatic morpho annotation (lemma+tag)
-- Manual syntax annotation (subject/predicate/object span)
-- Automatic syntax annotation (parent+fnc)
-- ??? Error annotation ???

Manual Morph
    doc
        para
            token + lemma + tag
             
Auto morphodita
    token + lemma + tag + parent + syn fnc

Merged Brat:
    token + todo

"""


# def process(manual_mFile, auto_morph_syn_file, manual_synFile, )
class BratToken(NamedTuple):
    idx: int
    offset: int
    value: str
    synFnc: str = None
    synLemma: str = None



class Token(NamedTuple):
    idx: int
    offset: int
    value: str
    man_mtag: str = None
    man_lemma: str = None
    auto_mtag: str = None
    auto_lemma: str = None
    man_synFnc: str = None
    man_synLemma: str = None
    auto_synFnc: str = None




def getSyn(annotFile):
    offset2syn = {}

    with open(annotFile, 'r', encoding='utf8') as r:
        for line in r:
            if line:
                # T1	Predicate 6 13	přinesl
                parts = line.split()
                fnc = parts[1]
                offset = int(parts[2])
                token = parts[4]
                offset2syn[offset] = (fnc, token)

    return offset2syn


def mergeBrat(txtFile, annotFile) -> BratToken:
    with open(txtFile, 'r', encoding='utf8') as r:
        txt = r.read().replace('\n', ' ')

    vals = txt.split(' ')

    curOffset = 0
    tokens = []
    for val in vals:
        tokens.append((val, curOffset))
        curOffset += len(val) + 1

    offset2syn = getSyn(annotFile)

    idx = 1
    for token in tokens:
        if token[1] in offset2syn:
            syn = offset2syn.get(token[1])
            yield BratToken(idx=idx, offset=token[1], value=token[0], synFnc=syn[0], synLemma=syn[1])
        else:
            yield BratToken(idx=idx, offset=token[1], value=token[0])
        idx += 1


def forNone(item, default: str = "") -> str:
    return str(item) if item else default

    outDir = 'c:/Users/jirka/data/projects/czels2/bb/czesl-full/data/from-sgt/'

    words = []
    id = None

    for line in sys.stdin:
        line = line.strip()
        if line.startswith("<div "):
            # flush the previous document
            if id:
                text = formatText(words)
                fileName = outDir + id + ".txt"
                with open(fileName, 'w', encoding='utf8') as w:
                    print(f'Writing to {fileName}')
                    print(text, file=w)
                words = []

            id = re.fullmatch(r'<div t_id=\"([^\"]+)\" .*>', line).group(1)
            saveMeta(outDir, id, line)
        elif line.startswith("<word "):
            word = re.fullmatch(r'<word [^>]+>([^<]+)<[^>]+>', line).group(1)
            words.append(word)
        elif  line.startswith("<s "):
            pass
        elif line in ['</s>', '</div>', '</doc>'] or line.startswith("<?xml ") or line.startswith("<doc "):
            pass
        else:
            print(f"Unexpected {line}")


INDIR = 'd:\\projects\\czels2\\morph-syn-annotation\\test1\\BRY_B9_001'

def main():
    bratTokens = [t for t in mergeBrat(INDIR + '.tokenized', INDIR + '.ann')]

    tokenIdx = 0
    with open(INDIR + 'all.w.mm.vert', 'r', encoding='utf8') as r:
        for line in r:
            line = line.strip()
            
            if line.startswith("<doc ") or line.startswith("<p "):
                print(line)
            elif line in ["</doc>", "</p>"]:
                print(line)
            elif line.startswith("<doc ") or line.startswith("<img "):
                print(line)
            else:
                parts = line.split('\t')
#                print(parts)
                token = parts[0]
                lt = parts[1].split()
                lemma = lt[0]
                tag = lt[1]

                #token = Token(idx=tokenIdx, lemma=lemm )                
                t = bratTokens[tokenIdx]

                print(
                    t.idx,
                    t.value,
                    t.offset,
                    forNone(lemma),
                    forNone(tag),
                    #forNone(t.auto_lemma),
                    #forNone(t.auto_mtag),
                    forNone(t.synFnc),
#                    forNone(t.synLemma),
                    forNone("auto_synFnc"),
                    sep='\t'
                )
                tokenIdx += 1

if __name__ == '__main__':
    main()
