# coding=utf-8
# Python 3

"""
Converts sgt version of the czesl corpus into 
* series of plain text files, each with metadata files
* vertical
vertical

todo: format metadata into proper xml
todo: save vertical


Does not check the input format, assumes it is valid and correct.

Unfortunately, sgt does not contain paragraph boundaries.

"""
import sys
import re
from typing import List


def saveMeta(outDir: str, id: str, line: str):
    '''
    saves metadata about a single document to a separate file`
    :param outDir: 
    :param id: id of the document
    :param line: the sgt tag containing metada
    '''
    with open(outDir + id + ".meta", 'w', encoding='utf8') as w:
        print(line, file=w)


def formatText(words: List[str]) -> str:
    '''
    Creates a text from a list of tokens, adding spaces as necessary.  
    :param words: list of tokens 
    :return: text
    '''
    text = " ".join(words)
    text = text.replace("&quot;", "\"")
    text = text.replace("&apos;", "\'")
    text = text.replace("&amp;", "&")
    while (True):
        text1 = re.sub(r' " ([^"]+) " ', r' "\1" ', text, count=1)
        if text1 == text: break
        text = text1
    text = re.sub(r'([.!?]) ([.!?])', r'\1\2', text)
    text = re.sub(r' ([,.;:!?]+)', r'\1', text)
    text = re.sub(r'\( ', r'(', text)
    text = re.sub(r' \)', r')', text)

    return text


def main():
    outDir = 'c:/Users/jirka/data/projects/czels2/bb/czesl-full/data/from-sgt/'

    words = []
    id = None

    for line in sys.stdin:
        line = line.strip()
        if line.startswith("<div "):
            # flush the previous document
            if id:
                text = formatText(words)
                fileName = outDir + id + ".txt"
                with open(fileName, 'w', encoding='utf8') as w:
                    print(f'Writing to {fileName}')
                    print(text, file=w)
                words = []

            id = re.fullmatch(r'<div t_id=\"([^\"]+)\" .*>', line).group(1)
            saveMeta(outDir, id, line)
        elif line.startswith("<word "):
            word = re.fullmatch(r'<word [^>]+>([^<]+)<[^>]+>', line).group(1)
            words.append(word)
        elif  line.startswith("<s "):
            pass
        elif line in ['</s>', '</div>', '</doc>'] or line.startswith("<?xml ") or line.startswith("<doc "):
            pass
        else:
            print(f"Unexpected {line}")

if __name__ == '__main__':
    main()
